<?php

// Include the php dom parser
include_once 'simple_html_dom.php';

header('Content-type: application/json');

// Create DOM from URL or file

$html = file_get_html('http://www.animemobile.com/ongoing');

$data = [];
foreach($html->find('div.content-box') as $items)
{
  $itemBox = new \stdClass();
    foreach($items->find('div.content-right') as $li)
    $itemBox->Name= $li->find('h2',0)->plaintext;
    foreach($items->find('div.cover') as $li)
    $findA = $li->find('a',0)->href;
    $itemBox->Link= $findA;
    $findImg = $li->find('img',0)->src;
    $itemBox->Image= $findImg;
    $data[]= $itemBox;


}
echo json_encode($data);

?>