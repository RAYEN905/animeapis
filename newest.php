<?php

// Include the php dom parser
include_once 'simple_html_dom.php';

header('Content-type: application/json');

// Create DOM from URL or file

$html = file_get_html('http://www.animemobile.com/');

$data = [];
foreach($html->find('div.list-box') as $items)
{
    $itemBox = new \stdClass();
    foreach($items->find('div.list-item') as $li)
        $itemBox->Name= ltrim(rtrim(strip_tags($li->innertext)));

  
    foreach($items->find('a') as $li)
        $itemBox->ItemLink=$li->href;
    $data["items"][]= $itemBox;

}
echo json_encode($data);

?>